﻿using Calc.Core.ServiceInterfaces.Operators;

namespace Calc.Core.Domain
{
    public class SubtractOperator : IBinaryOperator
    {
        public string Symbol => "-";

        public int Priority => 1;

        public decimal Exec(decimal left, decimal right)
        {
            throw new System.NotImplementedException();
        }
    }
}
