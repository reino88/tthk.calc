import { Component } from '@angular/core';
import { Http, RequestOptionsArgs, Headers } from "@angular/http";

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent {

    public value: string = "";
    public expression: string = "";

    public numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
    public operators = ["+", "-", "x", "/"];

    constructor(private _http: Http) {

    }

    public calc() {

        var headers: RequestOptionsArgs = {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        };

        this._http.post("api/calculator", '"' + this.expression + '"', headers)
            .subscribe(x => {
                this.value = x.text();
            });
    }
}
